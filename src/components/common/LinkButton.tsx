import * as React from 'react';

import { Link } from 'react-router-dom';

interface ILinkButtonProps {
  buttonText: string,
  buttonPath: string
}

export const LinkButton = (props: ILinkButtonProps) => {
  return <div>
      <Link className="button special" to={props.buttonPath}>{props.buttonText}</Link>
    </div>
}
