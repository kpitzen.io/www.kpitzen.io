import * as React from 'react';
import * as ReactMarkdown from 'react-markdown';
import { connect } from 'react-redux';
import { Action, Dispatch } from 'redux';

import { Banner } from '../common/Banner';
import { LinkButton } from '../common/LinkButton';

import { LoadBlogPostAction } from '../../actions/blogPostLongActions';
import { RootState } from '../../reducers';
import { IBlogPost, IRenderedBlogPost } from '../../types';


interface IBlogPostLongProps {
  blogPosts: IBlogPost[];
  blogPost: IRenderedBlogPost;
  match: {
    params: {
      id: string
    }
  },
  loadBlogPost: (id: string) => void;
}

interface IBlogPostLongState {
  blogTitle: string;
  subTitle: string;
  nextPost?: IBlogPost;
  previousPost?: IBlogPost;
  id?: string;
  content?: string;
}

class BlogPostLong extends React.Component<IBlogPostLongProps, IBlogPostLongState> {

  public static getDerivedStateFromProps(props: IBlogPostLongProps, state: IBlogPostLongState) {
    const blogPostData = props.blogPosts.filter(blogPost => blogPost.filename === props.match.params.id)[0];
    const nextPost = props.blogPosts.filter(blogPost => blogPost.key - 1 === blogPostData.key)[0]
    const previousPost = props.blogPosts.filter(blogPost => blogPost.key + 1 === blogPostData.key)[0]
    return {
      blogTitle: blogPostData.title,
      nextPost,
      previousPost
    }
  }


  constructor(props: IBlogPostLongProps) {
    super(props);

    this.state = {
      blogTitle: "",
      subTitle: ""
    };

    const { id } = props.match.params;

    this.loadDynamicData(id);
  }

  public componentDidUpdate(prevProps: IBlogPostLongProps, prevState: IBlogPostLongState) {
    const { id } = this.props.match.params;
    if (id !== this.state.id) {
      this.loadDynamicData(id);
    }
  }

  public loadDynamicData = async (id: string) => {
    const url: string = await import(/* webpackMode: lazy */ `!file-loader!../../assets/pages/${id}/index.md`);
    const response = await fetch(url);
    const content = await response.text();
    this.setState({ id, content });
  };


  public render() {
    return(
      <div>
        <Banner headerText={this.state.blogTitle} paragraphText={this.state.subTitle} />
        <section id="one" className="wrapper">
          <div className="inner">
            <div className="flex">
              <ReactMarkdown source={this.state.content || ''} />
            </div>
          </div>
          <div className="flex row">
            {
              this.state.previousPost &&
              <LinkButton buttonText="Previous Post" buttonPath={'/blog/' + this.state.previousPost.filename}/>
            }
            {
              this.state.nextPost &&
              <LinkButton buttonText="Next Post" buttonPath={'/blog/' + this.state.nextPost.filename}/>
            }
          </div>
        </section>
      </div>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  return {
    blogPosts: state.blogPosts,
    blogPost: state.blogPost
  }
};

const mapDispatchToProps = (dispatch: Dispatch<Action>) => {
  return {
    loadBlogPost: (id: string) => {
      dispatch(LoadBlogPostAction(id));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BlogPostLong);
