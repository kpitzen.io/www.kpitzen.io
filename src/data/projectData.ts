export const currentProjects = [
  {
    key: 0,
    name: "GitLab AWS Key Rotator",
    description: "Automated Key Generation for GitLab and AWS",
    moreUrl: "https://gitlab.com/kpitzen.io/gitlab-aws-key-rotator"
  },
  {
    key: 1,
    name: "Flint",
    description: "Small side-scrolling platformer built in Unreal Engine 4 with C++",
    moreUrl: "https://kpitzen.github.io/Flint/"
  },
  {
    key: 2,
    name: "Hyena",
    description: "Modern CMS for Modern Web Development",
    moreUrl: "https://hyenacms.com/"
  },
]
