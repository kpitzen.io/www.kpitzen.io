variable "environment" {
  default     = "dev"
  description = "Environment to deploy to"
}

variable "subdomain" {
  default     = "dev"
  description = "Subdomain for which to provision ACM certificate"
}

provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "kpitzen-ci"
    key    = "kpitzen.io.tf"
    region = "us-east-1"
  }
}

resource "aws_s3_bucket" "bucket" {
  bucket = "${var.environment}-kpitzen.io"
  acl    = "private"

  tags {
    Name        = "kpitzen.io"
    Environment = "${var.environment}"
  }
}

locals {
  s3_origin_id = "kpitzen_s3_${var.environment}_origin"
}

resource "aws_cloudfront_distribution" "distribution" {
  origin {
    domain_name = "${aws_s3_bucket.bucket.bucket_regional_domain_name}"
    origin_id   = "${local.s3_origin_id}"

    s3_origin_config {
      origin_access_identity = "${aws_cloudfront_origin_access_identity.identity.cloudfront_access_identity_path}"
    }
  }

  comment = "Cloudfront Distribution for ${var.environment} kpitzen.io"

  enabled = true

  default_root_object = "index.html"

  aliases = ["${var.subdomain}.kpitzen.io"]

  default_cache_behavior {
    allowed_methods        = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = "${local.s3_origin_id}"
    viewer_protocol_policy = "redirect-to-https"

    forwarded_values {
      cookies {
        forward = "all"
      }

      query_string = true
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn      = "${aws_acm_certificate.cert.arn}"
    minimum_protocol_version = "TLSv1"
    ssl_support_method       = "sni-only"
  }

  custom_error_response {
    error_code         = 404
    response_code      = 200
    response_page_path = "/index.html"
  }

  custom_error_response {
    error_code         = 403
    response_code      = 200
    response_page_path = "/index.html"
  }

  tags {
    Name        = "kpitzen.io"
    Environment = "${var.environment}"
  }
}

resource "aws_cloudfront_origin_access_identity" "identity" {
  comment = "${var.environment} Access Identity"
}

data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.bucket.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = ["${aws_cloudfront_origin_access_identity.identity.iam_arn}"]
    }
  }

  statement {
    actions   = ["s3:ListBucket"]
    resources = ["${aws_s3_bucket.bucket.arn}"]

    principals {
      type        = "AWS"
      identifiers = ["${aws_cloudfront_origin_access_identity.identity.iam_arn}"]
    }
  }
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = "${aws_s3_bucket.bucket.id}"
  policy = "${data.aws_iam_policy_document.s3_policy.json}"
}

resource "aws_acm_certificate" "cert" {
  domain_name = "${var.subdomain}.kpitzen.io"

  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

output "current_distribution_domain_name" {
  value = "${aws_cloudfront_distribution.distribution.domain_name}"
}

output "certificate_dns_validation" {
  value = "${aws_acm_certificate.cert.domain_validation_options}"
}
